//--------------------------------------------------------------------------------------
//Alias
//--------------------------------------------------------------------------------------
var KanaX_alias_Sprite_Character_Update = Sprite_Character.prototype.update;
var KanaX_alias_Sprite_Character_UpdateOther = Sprite_Character.prototype.updateOther;
var KanaX_alias_Game_Character_Init = Game_Character.prototype.initialize;
var KanaX_alias_Game_Map_Erase = Game_Map.prototype.erase;
var KanaX_alias_Game_Character_Update = Game_Character.prototype.update;
var KanaX_alias_Sprite_Character_InitMembers = Sprite_Character.prototype.initMembers;
//--------------------------------------------------------------------------------------
// Game_Character
//--------------------------------------------------------------------------------------

Game_Character.prototype.initialize = function() {
	KanaX_alias_Game_Character_Init.call(this);
    this.initRotation();
};
Game_Character.prototype.angle = function() {
    return this._angle;
};
Game_Character.prototype.initRotation = function() {
    this._angle = 0;
    this._rotationSpeed = 0;
};
Game_Character.prototype.rotate = function(speed) {
    this._rotationSpeed = speed;
};
Game_Character.prototype.update = function() {
	KanaX_alias_Game_Character_Update.call(this);
    this.updateRotation();
};
Game_Character.prototype.updateRotation = function() {
    if (this._rotationSpeed !== 0) {
        this._angle += this._rotationSpeed / 2;
    }
};

//--------------------------------------------------------------------------------------
// Sprite_Character
//--------------------------------------------------------------------------------------
Sprite_Character.prototype.initMembers = function() {
	KanaX_alias_Sprite_Character_InitMembers.call(this);
	this.anchorX = 0.5;
	this.anchorY = 1;
}

Sprite_Character.prototype.update = function() {
	KanaX_alias_Sprite_Character_Update.call(this);
	this.updateOther();
};
Sprite_Character.prototype.updateOther = function() {
	KanaX_alias_Sprite_Character_UpdateOther.call(this);
	var character = this._character;
	this.rotation = character.angle() * Math.PI / 180;
	this.anchor.x = character.anchorX;
	this.anchor.y = character.anchorY;

};

//--------------------------------------------------------------------------------------
// Game_Map
//--------------------------------------------------------------------------------------

Game_Map.prototype.rotateEvent = function(eventId, speed) {
    var character = this._events[eventId];
    if (character) {
    	character.rotate(speed);
    };
};
Game_Map.prototype.anchorSet = function(eventId, anchorX, anchorY) {
    var character = this._events[eventId];
    if (character) {
    	character.anchor = anchorX;
    	character.anchor = anchorY;
    };
};


//--------------------------------------------------------------------------------------
// Game_Event
//--------------------------------------------------------------------------------------
Game_Event.prototype.erase = function() {
	KanaX_alias_Game_Map_Erase.call(this);
    this.initRotation();
};

